FROM node:10

RUN npm install -g @angular/cli

RUN mkdir /app
WORKDIR /app

EXPOSE 4200

ENTRYPOINT ["ng","serve","--host","0.0.0.0"]
